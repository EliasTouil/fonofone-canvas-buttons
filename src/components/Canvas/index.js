import React, { Component } from "react";
import Konva from "konva";
import { Stage, Layer, Star, Text } from "react-konva";

const { sqrt, round } = Math;
const MINI_TRIANGLE_RATIO = 1 / 8;
class Canvas extends Component {
  state = {
    triangleData: null
  };
  handleDragStart = e => {
    //.. set some values
    // e.target.setAttrs({
    //   fill: "#ffffff"
    // });
  };
  handleDragEnd = e => {
    //animate..
    // e.target.to({
    //   duration: 0.5,
    //   easing: Konva.Easings.ElasticEaseOut
    // });
    // this is in docs but doesn't work (handleDrag attributes don't change)
    // e.target.setAttrs({
    //   fill: "#444444"
    // });
  };

  handleXChange = e => {
    let {
      bckTriangleSide,
      bckTriangleHeight,
      miniTriangleSide,
      miniTriangleHeight,
      bckTriangleBottomVerticeToXzero
    } = this.state.triangleData;
    let bckTriangleToMiniTriangleCenterRatio =
      bckTriangleHeight / (bckTriangleHeight - miniTriangleHeight);

    let relativeValue =
      (((e.newVal - miniTriangleSide / 2 - bckTriangleBottomVerticeToXzero) *
        bckTriangleToMiniTriangleCenterRatio) /
        bckTriangleSide) *
      100;

    this.props.setValues(v => ({ ...v, x: round(relativeValue) }));
  };
  handleYChange = e => {
    let {
      miniCircumscribedCircleRadius,
      bckTriangleHeight,
      miniTriangleHeight
    } = this.state.triangleData;

    let bckTriangleToMiniTriangleCenterRatio =
      bckTriangleHeight / (bckTriangleHeight - miniTriangleHeight);

    let relativeValue =
      (((e.newVal - miniCircumscribedCircleRadius) *
        bckTriangleToMiniTriangleCenterRatio) /
        bckTriangleHeight) *
      100;

    this.props.setValues(v => ({
      ...v,
      y: round(relativeValue)
    }));
  };

  calculateTriangles = nodeWidth => {
    // To see the relation between the node and the triangle try setting the star's number of points to 1000
    let canvasNodeWidth = nodeWidth / (sqrt(3) / 2);
    // This is the canvas' node "box model" height
    let canvasNodeHeight = canvasNodeWidth;
    let bckTriangleSide = nodeWidth;

    // Building the Star node inside a square will leave a white space to the left and the right
    let bckTriangleBottomVerticeToXzero =
      (canvasNodeWidth - bckTriangleSide) / 2;
    let bckTriangleHeight = bckTriangleSide * (sqrt(3) / 2);
    let bckTriangleCircumscribedCircleRadius = bckTriangleSide * (sqrt(3) / 3);
    let miniTriangleWidth = canvasNodeWidth * MINI_TRIANGLE_RATIO;
    let miniTriangleSide = bckTriangleSide * MINI_TRIANGLE_RATIO;

    let miniTriangleHeight = miniTriangleSide * (sqrt(3) / 2);
    let miniCircumscribedCircleRadius =
      bckTriangleCircumscribedCircleRadius * MINI_TRIANGLE_RATIO;
    let miniInscribedCircleRadius = miniCircumscribedCircleRadius / 2;
    let miniTriangleBottomVerticeToXzero =
      bckTriangleBottomVerticeToXzero * MINI_TRIANGLE_RATIO;

    let triangleData = {
      canvasNodeWidth,
      canvasNodeHeight,
      bckTriangleBottomVerticeToXzero,
      bckTriangleHeight,
      bckTriangleCircumscribedCircleRadius,
      bckTriangleSide,
      miniTriangleWidth,
      miniTriangleSide,
      miniTriangleHeight,
      miniCircumscribedCircleRadius,
      miniInscribedCircleRadius,
      miniTriangleBottomVerticeToXzero
    };

    return triangleData;
  };

  constructor(props) {
    super(props);
    this.state = {
      triangleData: this.calculateTriangles(this.props.width)
    };
  }
  componentDidMount() {
    this.star.on("xChange", e => this.handleXChange(e));
    this.star.on("yChange", e => this.handleYChange(e));
  }

  render() {
    if (this.state.triangleData) {
      let {
        canvasNodeWidth,
        canvasNodeHeight,
        bckTriangleBottomVerticeToXzero,
        bckTriangleHeight,
        bckTriangleSide,
        miniTriangleSide,
        miniCircumscribedCircleRadius,
        miniInscribedCircleRadius
      } = this.state.triangleData;

      let distFromLeftBound = ({ x, y }) => {
        let bottomLeftVertexX =
          x - miniTriangleSide / 2 - bckTriangleBottomVerticeToXzero;
        let bottomLeftVertexY = y + miniInscribedCircleRadius;
        let bottomLeftVertexXToBoundX =
          (bottomLeftVertexY * (bckTriangleSide / 2)) / bckTriangleHeight;

        let rtrn =
          bottomLeftVertexX - (bckTriangleSide / 2 - bottomLeftVertexXToBoundX);

        return rtrn;
      };
      let distFromRightBound = ({ x, y }) => {
        let bottomRightVertexX =
          x + miniTriangleSide / 2 - bckTriangleBottomVerticeToXzero;
        let bottomRightVertexY = y + miniInscribedCircleRadius;
        let bottomRightVertexXToBoundX =
          (bottomRightVertexY * (bckTriangleSide / 2)) / bckTriangleHeight;

        let rtrn =
          -1 *
          (bottomRightVertexX -
            (bckTriangleSide / 2 + bottomRightVertexXToBoundX));
        return rtrn;
      };
      let triangleDragBound = ({ x, y }) => {
        let correctedPos = { x, y };
        let overTop = y - miniCircumscribedCircleRadius < 0;
        if (overTop) {
          y = miniCircumscribedCircleRadius;
        }

        let underBottom = y > bckTriangleHeight - miniInscribedCircleRadius;
        if (underBottom) {
          y = bckTriangleHeight - miniInscribedCircleRadius;
        }

        if (distFromLeftBound({ x, y }) < 0) {
          correctedPos.x = x - distFromLeftBound({ x, y });
        }

        if (distFromRightBound({ x, y }) < 0) {
          correctedPos.x = x + distFromRightBound({ x, y });
        }

        correctedPos.y = y;

        return correctedPos;
      };

      return (
        <Stage width={canvasNodeWidth} height={canvasNodeHeight}>
          <Layer>
            <Star
              ref={node => (this.bck = node)}
              x={canvasNodeWidth / 2}
              y={canvasNodeWidth / 2}
              numPoints={3}
              innerRadius={canvasNodeWidth / 2 / 2}
              outerRadius={canvasNodeWidth / 2}
              fill="#e1c800"
              opacity={1}
              stroke="#ffe300
							"
              strokeWidth={4}
              lineJoin="round"
              lineCap="round"
              cornerRadius={100}
            />
            <Star
              ref={node => (this.star = node)}
              x={canvasNodeWidth / 2}
              y={miniCircumscribedCircleRadius}
              numPoints={3}
              innerRadius={miniCircumscribedCircleRadius / 2}
              outerRadius={miniCircumscribedCircleRadius}
              fill="#ffe300"
              opacity={1}
              draggable
              stroke="#FFFFFF"
              strokeWidth={4}
              onDragStart={this.handleDragStart}
              onDragEnd={this.handleDragEnd}
              dragBoundFunc={triangleDragBound}
              lineJoin="round"
              lineCap="round"
              cornerRadius={100}
              perfectDrawEnabled
            />
          </Layer>
        </Stage>
      );
    }

    return <div> Chargement </div>;
  }
}

export default Canvas;
