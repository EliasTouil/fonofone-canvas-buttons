import React, { useState } from "react";
import Canvas from "./components/Canvas";

function App() {
  let [values, setValues] = useState({ x: null, y: null });
  return (
    <>
      <Canvas width={window.innerWidth} setValues={setValues} />
      <div style={{ fontSize: "64px" }}>
        <p>x:{values.x}</p>
        <p>y:{values.y}</p>
      </div>
    </>
  );
}

export default App;
